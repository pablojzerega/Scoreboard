
// Incluir bibliotecas
  #include <OneButton.h>
  #include <EEPROM.h>
  #include<math.h>  
// Definir pines
  // SN74HC
    #define dataSN 10
    #define latchSN 11
    #define clockSN 12

  // 4 digites del seven segment
    #define digito1 3
    #define digito2 5
    #define digito3 6
    #define digito4 9

  // Botones del transmisor/receptor
    #define boton1 A0
    #define boton2 A1
    #define boton3 A2
    #define boton4 A3

    OneButton boton_1(A3, true);
    OneButton boton_2(A4, true);    
    OneButton boton_3(A1, true); 
    OneButton boton_4(A2, true);

// Definir variables
  // Variables de puntaje
    int puntajeJ1;
    int puntajePrevioJ1;
    int puntajeJ2;
    int puntajePrevioJ2;
    int puntajeConcatenado;
    int jugadorElegido = 0;
    int ultimoJugador = 0;

  // Variables del 4 digits segment ;

    int puntajeScoreboard []{63,63,63,63};

// Metodos de puntaje

  void sumarPunto(int jugadorElegido){
    if (jugadorElegido == 1) {
      puntajeJ1++;
      ultimoJugador = 1;
    } else if (jugadorElegido == 2){
      puntajeJ2++;
      ultimoJugador = 2;
    }
  }
  void restarPunto(int ultimoJugador){
    if (ultimoJugador == 1) {
        puntajeJ1--;
      } else if (ultimoJugador == 2){
        puntajeJ2--;
      }
  }
  void resetearContador(){
    puntajeJ1 = 0;
    puntajeJ2 = 0;
    ultimoJugador = 0;
  }
  void actualizarPuntaje(){
  }

// Metodos de display
  // 1 - Concatenar puntajes en variable puntajeConcatenado
  // 2 - Funcion numeroBinario traduce del 0 a 9 a binario
  // 3 - Funcion digitoSN74 envia el numero binario al SN74 (que segmentos se prenden)
  // 4 - Funcion prenderDigito elige que digito se prende
  // 5 - Funcion dividirPuntaje divide puntajeConcatenado en 4 digitos
  // 6 - Funcion mostrarPuntaje

  int puntajeFinal(int PJ1, int PJ2)
    {
      int puntajeConcatenado;
      puntajeConcatenado = PJ1*100 + PJ2;
      return puntajeConcatenado;
    // Si puntajeConcatenado tiene 1 digito -- PJ1 = 0 y PJ2 >0 y < 10 -- D1, D2 y D3 0
    // Si puntajeConcatenado tiene 2 digitos -- PJ1 = 0 y PJ2 > 10  -- D1 y D2 0
    // Si puntajeConcatenado tiene 3 digitos -- PJ1 >0                   -- D1 0
    // Si puntajeConcatenado tiene 4 digitos -- PJ1 y PJ2 > 9    
    }
  int numeroBinario(int numero){
    switch (numero){ // traduce numeros a binario
      case 0:
        return 63;
        break;

      case 1:
        return 6;
        break;

      case 2:
        return 91;
        break;

      case 3:
        return 79;
        break;

      case 4:
        return 102;
        break;

      case 5:
        return 109;
        break;

      case 6:
        return  124;
        break;

      case 7:
        return 7;
        break;

      case 8:
        return 127;
        break;

      case 9:
        return 103;
        break;                                                               
    }
  }
  void digitoSN74 (int numero){
      int numeroBin;
      numeroBin = numeroBinario(numero);
      digitalWrite(latchSN, LOW); 
      shiftOut(dataSN, clockSN, LSBFIRST,numeroBin); 
      digitalWrite(latchSN, HIGH); 
  }

  void prenderDigito(int digito) {
    switch (digito){
      case 1:
        digitalWrite(digito1, HIGH);
        digitalWrite(digito2, LOW);
        digitalWrite(digito3, LOW);
        digitalWrite(digito4, LOW);
        break;

      case 2:
        digitalWrite(digito1, LOW);
        digitalWrite(digito2, HIGH);
        digitalWrite(digito3, LOW);
        digitalWrite(digito4, LOW);
        break;

      case 3:
        digitalWrite(digito1, LOW);
        digitalWrite(digito2, LOW);
        digitalWrite(digito3, HIGH);
        digitalWrite(digito4, LOW);
        break;

      case 4:
        digitalWrite(digito1, LOW);
        digitalWrite(digito2, LOW);
        digitalWrite(digito3, LOW);
        digitalWrite(digito4, HIGH);
        break;
      
      default:
        digitalWrite(digito1, LOW);
        digitalWrite(digito2, LOW);
        digitalWrite(digito3, LOW);
        digitalWrite(digito4, LOW);
        break;                                  
    }
  }
  void mostrarDigitos(int digito, int numero){
      switch(digito){
        case 1:
          digitoSN74(numero);
          prenderDigito(1);
          break;

        case 2:
          digitoSN74(numero);
          prenderDigito(2);
          break;

        case 3:
          digitoSN74(numero); 
          prenderDigito(3);
          break;

        case 4:
          digitoSN74(numero);
          prenderDigito(4);
          break;

        default:
          prenderDigito(5);
          break;  
        }

  }


  int calcularDigitos(int num){
    int numDigitos;
    if(num == 0){
      numDigitos = 1;
      } else {
        numDigitos=log10(num)+1;  
    }
  }
  void dividirPuntaje (int puntajeConcatenado){
    int numDigitosPuntaje = calcularDigitos(puntajeConcatenado);
    if(puntajeConcatenado == 0){
      puntajeScoreboard[0] = 63;
      puntajeScoreboard[1] = 63;
      puntajeScoreboard[2] = 63;
      puntajeScoreboard[3] = 63;
    }

    if (numDigitosPuntaje == 1){//Poner 3 ceros en  1 2 y 3
      puntajeScoreboard[0] = 63;
      puntajeScoreboard[1] = 63;
      puntajeScoreboard[2] = 63;
      puntajeScoreboard[3] = puntajeConcatenado;
      }

    if (numDigitosPuntaje == 2) {
      puntajeScoreboard[0] = 63;
      puntajeScoreboard[1] = 63;
      puntajeScoreboard[2] = (puntajeConcatenado / 10U) % 10;
      puntajeScoreboard[3] = (puntajeConcatenado / 1U)%10;
      }
    if (numDigitosPuntaje == 3) {

      puntajeScoreboard[0] = 63;
      puntajeScoreboard[1] = (puntajeConcatenado / 100U) % 10;
      puntajeScoreboard[2] = (puntajeConcatenado / 10U) % 10;
      puntajeScoreboard[3] = (puntajeConcatenado / 1U)%10;
      }
    
    if (numDigitosPuntaje == 4) { 
      puntajeScoreboard[0] = (puntajeConcatenado / 1U) %10;
      puntajeScoreboard[1] = (puntajeConcatenado / 100U) % 10;
      puntajeScoreboard[2] = (puntajeConcatenado / 10U) % 10;
      puntajeScoreboard[3] = (puntajeConcatenado / 1U)%10;
      }
    }

  void mostrarPuntaje(int puntajeConcatenado){
    long beginTime = millis();


    if (puntajeConcatenado == 0){
      mostrarDigitos(1,puntajeScoreboard[0]);
      while( (millis() - beginTime) < 10);
      mostrarDigitos(2,puntajeScoreboard[1]);
      while( (millis() - beginTime) < 13);
      mostrarDigitos(3,puntajeScoreboard[2]);
      while( (millis() - beginTime) < 17) ;      
      mostrarDigitos(4,puntajeScoreboard[3]);
      while( (millis() - beginTime) < 21) ;       
    }
  
    if (numDigitosPuntaje == 1){//Poner 3 ceros en  1 2 y 3
     mostrarDigitos(1,puntajeScoreboard[0]);
      while( (millis() - beginTime) < 10);
      mostrarDigitos(2,puntajeScoreboard[1]);
      while( (millis() - beginTime) < 13);
      mostrarDigitos(3,puntajeScoreboard[2]);
      while( (millis() - beginTime) < 17) ;      
      mostrarDigitos(4,puntajeScoreboard[3]);
      while( (millis() - beginTime) < 21) ;  
      }

    if (numDigitosPuntaje == 2) {
      mostrarDigitos(1,puntajeScoreboard[0]);
      while( (millis() - beginTime) < 10);
      mostrarDigitos(2,puntajeScoreboard[1]);
      while( (millis() - beginTime) < 13);
      mostrarDigitos(3,puntajeScoreboard[2]);
      while( (millis() - beginTime) < 17) ;      
      mostrarDigitos(4,puntajeScoreboard[3]);
      while( (millis() - beginTime) < 21) ;  
      }

    if (numDigitosPuntaje == 3) {
      mostrarDigitos(1,puntajeScoreboard[0]);
      while( (millis() - beginTime) < 10);
      mostrarDigitos(2,puntajeScoreboard[1]);
      while( (millis() - beginTime) < 13);
      mostrarDigitos(3,puntajeScoreboard[2]);
      while( (millis() - beginTime) < 17) ;      
      mostrarDigitos(4,puntajeScoreboard[3]);
      while( (millis() - beginTime) < 21) ;  
      }
    
    if (numDigitosPuntaje == 4) { 
      mostrarDigitos(1,puntajeScoreboard[0]);
      while( (millis() - beginTime) < 10);
      mostrarDigitos(2,puntajeScoreboard[1]);
      while( (millis() - beginTime) < 13);
      mostrarDigitos(3,puntajeScoreboard[2]);
      while( (millis() - beginTime) < 17) ;      
      mostrarDigitos(4,puntajeScoreboard[3]);
      while( (millis() - beginTime) < 21) ;  
      }
   
  }

// Botones

  void boton1_singleclick(){ // Suma punto J1
    sumarPunto(1);
  }

  void boton2_singleclick(){ // Suma punto J2
    sumarPunto(2);
  }

  void boton3_singleclick(){ // Resta punto al ultimo que sumo
    restarPunto(ultimoJugador);
  }

  void boton4_singleclick(){ // Resetea
    resetearContador();
  }



// Setup
  void setup() {
  puntajeJ1 = 0;
  puntajeJ2 = 0;
  }

// Loop
  void loop() {
    boton_1.tick();
    boton_2.tick();
    boton_3.tick();
    boton_4.tick();

    if (puntajeJ1 != puntajePrevioJ1 || puntajeJ2 != puntajePrevioJ2){
      actualizarPuntaje();
    }
    mostrarPuntaje(puntajeConcatenado);    
  }
